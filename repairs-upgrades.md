
# Creality CR-10S Pro V2 Repairs and Upgrades


## Introduction

These notes are a summary of the repairs and upgrades I've made to my
[Creality CR-10S Pro V2](https://www.creality3dofficial.com/products/cr-10s-pro-v2-3d-printer) 3D printer, purchased in late 2020.
They are primarily for my own records, but may also be useful to other CR-10S
Pro V2 owners.


## Disclaimer

The repairs I've described below are caused by design or manufacturing flaws
with my CR-10S Pro V2 unit. Other owners of this printer have reported
different (or no) repair issues, so your experience with this printer may be
very different.

The upgrades listed below are for ergonomics and convenience; other CR-10S Pro
V2 owners may choose to make different upgrades.


## Printer terminology

I may have used incorrect terminology for some of the printer components and
operations below. Please let me know of any errors.

In what follows, "left" and "right" printer components refer to the printer as
viewed from the "front", where the touch display is mounted.


## Repair: Z-axis stepper motor connectors

After initial assembly, I noticed the right hand Z-axis rod and stepper motor
were lagging badly behind the left-hand level. On closer inspection, the
connector cable for the right-hand Z-axis stepper was looped up above the case
and the print bed had caught on it and pulled it out.


### Remediation

Push the slack in the left and right Z-axis stepper motor cables back into the
slots in the electronics case. Take care not to put extra bending strain on
the stepper motor cable connectors.


## Repair: Z-axis threaded rods binding

After initial assembly, the right-hand Z-axis threaded rod seemed to be
binding up and not moving as freely as the left.


### Remediation

Loosen the couplers for both Z-axis threaded rods and remove them. Check by
rolling on a known-flat surface that both rods are straight. Reinstall the
rods, ensuring they are free to rotate as they drop back own to the Z-axis
couplers. Re-tighten the couplers onto the rods.


## Repair: X-axis gantry lagging on the left

The CR-10S Pro V2 has well-known issues keeping the X-axis gantry level with
respect to the printer frame.

The X-axis gantry rides on the left and right Z-axis threaded rods. If these
rods move out of synchronization the extra weight on the left hand side of the
X-axis gantry causes it to "lag" the right side. This is particularly
noticeable when the Z-axis stepper motors are powered off.


### Remediation

Install an aftermarket [Z axis synchronization kit](https://tiny-machines-3d.myshopify.com/products/z-sync-kit-1) across the
tops of both Z-axis threaded rods. This should help the X axis gantry move
uniformly on both Z-axis rods.

Then [mechanically level](https://www.youtube.com/watch?v=qv_kZrL5uiw) the X axis gantry using the supplied
plastic shim or some other device.


### Notes

Even with the Z-axis synchronization belt I've found after every couple of
days the X-axis gantry needs to be manually re-leveled. I do this by turning
off the power, setting blocks of equal height under the gantry at each end and
manually turning the Z-axis couplers until the gantry is re-level.

Some owners have remediated this design flaw by fitting
[anti-backlash nuts](https://www.amazon.com/Backlash-Elimination-Upgrade-Tornado-Threaded/dp/B08LZ1V56T) to both Z-axis threaded rods.


## Upgrade: improved print surface

For many 3D printer owners the choice of a print surface is a matter of
personal preference, so use a surface that works for you.


### Remediation

Purchase and fit an improved print surface for the CR-10S Pro V2. In my case
this was a [Creality glass bed](https://www.amazon.com/Creality-Tempered-Upgraded-Printing-310mmx320mmx4mm/dp/B0863SSDYT).


## Repair: Part cooling fan duct

Soon after printer assembly I noticed that the part fan cooling duct was
catching on the extruded filament and dragging the print across the bed. It
appears this cooling duct was a relatively poor quality 3D printed part and
had partly melted from hot-end radiated heat.


### Remediation

Purchase and fit a higher quality aftermarket [cooling duct](https://tiny-machines-3d.myshopify.com/products/replacement-fan-duct-for-creality-cr-10s-pro-and-cr-10-max).


## Upgrade: silicon bed spacers

The CR-10S Pro V2 comes from the factory with the "yellow" bed springs. On
previous printers I've had issues with these springs buckling and moving.


### Remediation

Purchase and fit silicon [bed spacers](https://www.aliexpress.com/item/32962997573.html) in place of the yellow
bed springs. Three of the spacers are the same size, ensure the fourth, longer
one is fitted to the back, left corner of the bed, around the cable
connection.


## Repair: impractically loud fans

With all fans running, the CR-10S Pro V2 is impractically loud. It needs to be
located in a dedicated space, or in a sound dampening enclosure.

After eight months of operation some of the internal case fans started to fail
on my CR-10S Pro V2, so I've listed this issue as a repair, rather than an
obvious upgrade.


### Remediation

Purchase and fit higher quality, lower noise aftermarket fans, for example
[this kit](https://arcto3d.se/product/cr-10s-pro-pro-v2-quiet-fan-kit-exhaust-hotend-psu-board-cooling-fan-kit/). This kit is not complete, you will need to supply some
small bolts and 3D print a fan adapter for the motherboard fan.

I bought a kit of fans for convenience, but you should be able to source
individual, higher quality fans too.


### Notes

A number of other CR-10S Pro V2 owners have elected to fit low-noise,
12V&#x2013;often Noctua&#x2013;fans to their printer. Most of the printer fans are 24V so
this means using a buck-converter or installing 12V fans in series on a 24V
circuit.

To avoid complications I've tried to replace like-with-like fans throughout,
however there are numerous references online to using 12V fans.

For reference, the fans installed in the CR-10S Pro V2 are:

-   1 x 4020 24V (DC) Axial &#x2013; Mounted internally in the case

-   1 x 4020 24V (DC) Radial &#x2013; Mounted internally in the case

-   1 x 4020 12V (DC) Axial &#x2013; Mounted internally in the power supply

-   1 x 4020 24V (DC) Radial &#x2013; Mounted on extruder

-   1 x 4010 24V (DC) Axial &#x2013; Mounted on extruder


## Repair: Bowden tube connectors detaching

The CR-10S Pro V2 has pneumatic connectors that hold the ends of the Bowden
tube to the extruder and hot-end. After a couple of months printing these
connectors were no longer able to hold the Bowden tube in place during
printing, causing print problems.


### Remediation

Purchase and fit higher quality pneumatic connectors to the ends of the Bowden
tube. For convenience I bought [a kit](https://www.ebay.com.au/itm/223758724249) that also includes
a spare PTFE Bowden tube, but you can buy the connectors individually.


## Repair: Y axis carriage wheels and belt

After some initial prints my CR-10S Pro V2 printer started to have trouble
moving the Y axis (including the build plate) smoothly. Shortly after that,
prints started to show very large offsets in the Y axis direction.

Upon inspection, the Y axis wheels appeared badly worn, and this was affecting
movement of the Y axis carriage. I believe the eccentric nuts that hold the Y
axis wheels to the axis extrusion were over-tightened and this had caused the
premature wear.

The Y axis belt also appeared to be over-tightened, placing too much strain on
the Y axis stepper motor and the Y axis idler wheel.


### Remediation

Purchase and fit [replacement wheels](https://www.ebay.com.au/itm/373214323005) for the Y axis.
Tighten&#x2013;but don't over-tighten&#x2013;the Y axis wheel eccentric nuts so the Y
carriage rides free and level on the Y axis aluminium extrusion.

Loosen (or tighten) the Y axis belt stepper motor and the Y axis idler pulley
until the belt is tight&#x2013;but not twanging like a guitar string. If the belt is
worn or too short, try fitting a [replacement timing belt](https://www.aliexpress.com/item/4000125408676.html?spm=a2g0s.12269583.0.0.473d7c90u8FqA4).


## Upgrade: SD card reader extender

The CR-10S Pro V2 has *two* SD card readers: one on the right side of the case
for loading G-code files, and a second one *inside* the case. The internal SD
card reader is for upgrading the touchscreen firmware, as this firmware is
(unfortunately) separate from the mainboard firmware.

So to upgrade or change the touchscreen firmware, you need to take the bottom
off the case to access this SD-card reader.


### Remediation

Purchase and fit an [SD card extension cable](https://www.ebay.com.au/itm/192052831023) and feed the thin end
of the cable through the ventilation slots on the left side of the case into
the SD card slot for the touchscreen.

You may want collect and wrap the excess extension cable so it doesn't get in
the way of printer operations. Now you should be able to access the screen SD
card reader as needed.


## Upgrade: metal leveling wheels

The CR-10S Pro V2 comes from the factory with functional black plastic
leveling wheels. Upgrading these is strictly a matter of personal preference.


### Remediation

Purchase and fit larger, [metal leveling wheels](https://www.amazon.com/Creality-Capricorn-Filament-Aluminum-Pneumatic/dp/B082PC59BP).


## Upgrade: new or better nozzle

3D print nozzles have a finite life, especially when used with more abrasive
filaments. Once the printer nozzle is due for replacement choose your
favourite compatible replacement nozzle.


### Remediation

On my CR-10S Pro V2 I've successfully used replacement nozzles from
[Micro Swiss](https://www.amazon.com/gp/product/B07NF66VJ3/) and [Bondtech](https://www.phaserfpv.com.au/products/bondtech-m6-nickel-coated-nozzle-creality-cr10s-pro-v2).


## Repair: False positives from filament run-out sensor

The CR-10S Pro V2 printer comes standard with a filament run-out sensor. After
several months of operation I found that I was seeing occasional filament
run-out false-positive alerts from the sensor.

In addition, having this sensor also means loading filament is a tedious
two-step process: in the first step align and insert the filament through the
sensor, and in the second step align and insert the filament through the
extruder. Both steps are prone to filament jamming or filament leaving the
ideal loading path.


### Remediation

Given the issues of false-positive filament run-outs and the extra hassle in
loading filament, I physically removed the sensor and unplugged it from the
ribbon cable breakout board. If the lack of run-out sensor becomes an issue in
the future, re-attaching it should be a straightforward procedure.


## Upgrade: Mainboard and touch screen firmware

The factory firmware for the CR-10S Pro V2 mainboard has functions for
managing prints, calibrating the printer and controlling heat and airflows.
The touch-screen firmware is, unfortunately, hosted on a separate, hard to
access processor.

Developers outside Creality have created their own mainboard and touch-screen
firmware packages which offer improvements in speed, precision or
capabilities. Two popular sources at the time of writing are the
[Tiny Machines Firmware](https://docs.google.com/document/d/1cuYWpDw8aPuLEXus3uNsgDIEAKEze3Z9-hIDMWcqMK8) or [Nic
Wilson's firmware](https://www.facebook.com/groups/485185272196044/files)


### Remediation

Choose a third party firmware for the CR-10S Pro V2 and follow the developer's
instructions for installation. I went with [Nic Wilson's
firmware](https://www.facebook.com/groups/485185272196044/files) and needed some [extra steps](https://www.reddit.com/r/CR10/comments/gqkdk2/fixing_firmware_update_issues_with_osx_catalina) on macOS for
formatting the touchscreen firmware SD card.

After the installation I re-calibrated the printer PID and extruder and have
made continued use of the upgraded 9x9 auto bed leveling (ABL) routine.


## Repair: cross-threaded nozzle and heat block

After wearing out and replacing several nozzles for the CR-10S Pro V2 hot-end,
I found it surprisingly difficult to install a new nozzle in place.

I'm uncertain of the cause, however, over time, the thread on the heat block
that accepts the nozzle may have become cross-threaded. Alternatively, the
heat block threads may have some obstructions that I wasn't able to clear&#x2013;for
example filament or metal fragments.


### Remediation

Purchase and fit a replacement heat block or entire hot-end for the CR-10S Pro
V2. On my printer I installed a like-for-like [replacement
hot-end](https://3dprintersonline.com.au/creality-assembled-extruder-hotend-with-capricorn-ptfe-tubing-for-cr-10s-pro/).


### Notes

When faced with hot-end issues some CR-10S Pro V2 owners have taken the
opportunity to upgrade the printer hot-end to a higher quality
[aftermarket unit](https://www.reddit.com/r/CR10/comments/i5m8ro).


## Upgrade: Start and end print G-CODE

Slicing a 3D geometry file into [G-code](https://en.wikipedia.org/wiki/G-code) for printing is the crucial
last step in preparing a print job for the CR-10S Pro V2. Slicers like
[Prusa slicer](https://www.prusa3d.com/page/prusaslicer_424/) and [Cura](https://ultimaker.com/software/ultimaker-cura) provide specialized
G-code sent to the printer at the start and end of each print.

I found that I wanted to tweak these G-codes to relocate the start print purge
lines and to provide more feedback on what the printer was doing. After
modifying these G-codes I updated the CR-10S Pro V2 profiles in each slicer
that I use in their respective "custom G-code" setting.

Here's the modified start-print G-code, based on the Prusa slicer start G-code
for the CR-10S Pro V2:


    ;;
    ;; Start print G-code.
    ;;
    ;; Adapted from Prusa slicer start-print G-code and other contributors.
    ;;
    ;; References:
    ;; * https://www.youtube.com/watch?v=nsRntOH_DdQ
    ;;

    M117 Preparing print...

    ; Use units in millimeters
    G21

    ; Use absolute coordinates
    G90

    ; Use extruder relative positioning
    M83

    ; Set bed levelling nozzle temperature
    M104 S190

    ; Set the heated bed temperature
    M140 S[first_layer_bed_temperature]

    ; Wait for the heated bed to reach temperature
    M190 S[first_layer_bed_temperature]

    ; Wait for the nozzle to reach levelling temperature
    M109 R190

    M117 Run auto bed levelling ...

    ; Do X levelling
    G34

    ; Home all axes (required before a G29)
    G28

    ; Run an auto-bed level
    G29

    ; Save and use the bed levelling mesh
    M420 S1

    M117 Print purge lines...

    ; Set nozzle printing temperature
    M104 S[first_layer_temperature]

    ; Wait for nozzle printing temperature
    M109 R[first_layer_temperature]

    ; Reset extruder position
    G92 E0

    ; Move the Z axis up
    G1 Z50 F240

    ; Small filament retraction to avoid dragging
    G92 E-2

    ; Move to the purge line start position
    G1 X3 Y12 F3000

    ; Set the Z axis offset
    G1 Z0.28 F240

    ; Print the first purge line
    G1 Y280 E15 F1500

    ; Move a little in the +X direction
    G1 X6 F5000

    ; Reset the extruder position
    G92 E0

    ; Print the second purge line
    G1 Y12 E15 F1200

    ; Reset the extruder position
    G92 E0

    M117 Starting print...

And the corresponding end-print G-code:


    ;;
    ;; End print G-code.
    ;;
    ;; Adapted from Prusa slicer end-print G-code and other contributors.
    ;;

    ; Move the print head up if needed.
    {if max_layer_z < max_print_height}
      G1 Z{z_offset + min(max_layer_z + 2, max_print_height)} F600
    {endif}

    ; Present the print.
    G1 X5 Y250 F{travel_speed*60}

    ; Move the print head further up.
    {if max_layer_z < max_print_height-10}
      G1 Z{z_offset + min(max_layer_z + 70, max_print_height - 10)} F600
    {endif}

    ; Turn the heatbed off
    M140 S0

    ; Turn the hot-end off
    M104 S0

    ; Turn the fan off
    M107

    ; Turn motors off
    M84 X Y E

    M117 Completed print.
