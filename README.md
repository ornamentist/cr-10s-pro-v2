
# The Creality CR-10S Pro V2 3D Printer

This repository contains a collection of notes on the
[Creality CR-10S Pro V2](https://www.creality3dofficial.com/products/cr-10s-pro-v2-3d-printer) 3D printer.

Most notes are provided in [Org mode](https://orgmode.org/) format and also in Markdown
format as Gitlab currently has errors in rendering Org file links.


## License

This document and accompanying files are licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International
License](https://creativecommons.org/licenses/by-sa/4.0).
